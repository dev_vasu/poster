import React from 'react';
import ReactDOM from 'react-dom';
import Root from './component/root';

import './style/main.scss';

var mountNode = document.getElementById('app');
ReactDOM.render(<Root />, mountNode);