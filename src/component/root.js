import React, { Component } from 'react';
import axios from 'axios'

import Router from './router';
import Error from './error';
import Loader from './loader';

import { AppStateProvider } from '../context/app-state'

class Root extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loadingState: 'LOADING',
            posts: [],
            users: {},
            searchText: '',
            currentPage: 0
        };
        this.updateUser = this.updateUser.bind(this);
        this.updatePage = this.updatePage.bind(this);
        this.updateSearchText = this.updateSearchText.bind(this);
    }

    updatePage(currentPage) {
        this.setState({ currentPage });
    }

    updateSearchText(searchText) {
        this.setState({ searchText });
    }

    updateUser(userId, data) {
        const newState = { ...this.state };
        newState.users[userId] = data;
        this.setState({ ...newState });
    }

    componentDidMount() {
        axios.get('https://jsonplaceholder.typicode.com/posts')
            .then((response) => {
                this.setState({
                    loadingState: 'LOADED',
                    posts: response.data
                });
            })
            .catch(() => {
                this.setState({ loadingState: 'ERROR' })
            });
    }

    render() {
        return (
            <AppStateProvider value={{
                posts: this.state.posts,
                users: this.state.users,
                updateUser: this.updateUser,
                currentPage: this.state.currentPage,
                updatePage: this.updatePage,
                searchText: this.state.searchText,
                updateSearchText: this.updateSearchText
            }}>
                <main>
                    {this.state.loadingState === 'LOADING' && <Loader />}
                    {this.state.loadingState === 'LOADED' && <Router />}
                    {this.state.loadingState === 'ERROR' && <Error message={'Unable to fetch posts'} />}
                </main>
            </AppStateProvider>
        );
    }
}

export default Root;
