import React from 'react';
import { MemoryRouter as Router, Route } from 'react-router-dom';

import PostSection from '../post-section';
import User from '../user';
import Post from '../post';

export default () => (
    <Router>
        <Route path='/' exact component={PostSection} />
        <Route path='/user/' component={User} />
        <Route path='/posts/:postId' component={Post} />
    </Router>
);
