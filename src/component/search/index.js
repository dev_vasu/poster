import React, { PureComponent } from "react";

class Search extends PureComponent {

    constructor(props) {
        super(props);
        this.state = { searchText: props.searchText };
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(event) {
        this.props.updateSearchText(event.target.value);
        this.setState({ searchText: event.target.value });
    }

    render() {
        return (
            <input type="text" value={this.state.searchText} onChange={this.handleChange} />
        );
    }

}

export default Search;
