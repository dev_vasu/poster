import React from 'react';

export default ({ count, currentPage, updatePage }) => {
    const list = new Array(count / 10).fill(0);

    return (
        <footer>
            <ul>
                {
                    list.map((i, index) => {
                        return (
                            <li
                                className={currentPage === index ? 'active' : ''}
                                key={index + 1}
                                onClick={() => { updatePage(index) }}
                            >
                                {index + 1}
                            </li>)
                    })
                }
            </ul>
        </footer>
    );
};
