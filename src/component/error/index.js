import React from "react";

export default ({ message }) => (
    <h1>{message}</h1>
);
