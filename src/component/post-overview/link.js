import React from 'react';
import { Link } from 'react-router-dom';

export default ({ title, pathname, data }) =>
    <Link to={{ pathname, data }}>{title}</Link>;
