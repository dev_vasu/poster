import React from 'react';
import CustomLink from './link';
import Loader from '../loader'

export default ({ postDetails, userDetails }) => {
    const userDetailsData = {
        postId: postDetails.id,
        title: postDetails.title,
        body: postDetails.body,
        userName: userDetails ? userDetails.name : ''
    };

    return (
        <li className='post'>
            <div>
                <p>{postDetails.title}</p>
                {
                    userDetails ?
                        <CustomLink title={userDetails.name} data={userDetails} pathname={'/user/'} /> :
                        <Loader />
                }
            </div>
            {
                userDetails ?
                    <CustomLink title='more' data={userDetailsData} pathname={`/posts/${postDetails.id}`} /> :
                    <Loader />
            }
        </li>
    );
};
