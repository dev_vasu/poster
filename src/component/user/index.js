import React from "react";
import { withRouter } from 'react-router-dom';

const User = (props) => {
    const { username, name, email, website, company } = props.location.data;
    return (
        <div className='user-info'>
            <ul>
                <li>{username}</li>
                <li>{name}</li>
                <li>{email}</li>
                <li>{website}</li>
                <li>{company.name}</li>
            </ul>
            <button onClick={() => props.history.push('/')}>Back to posts</button>
        </div>
    )
};
export default withRouter(User);