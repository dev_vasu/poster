import React from 'react';
import { withRouter } from 'react-router-dom';

import Comments from './comments';

import '../../style/post';

const Post = (props) => {
    const { title, body, userName } = props.location.data;

    return (
        <div className='post-details'>
            <p className='title'>{`${title} by ${userName}`}</p>
            <p className='content'>{body}</p>
            <Comments postId={props.match.params.postId} />
            <button onClick={() => props.history.push('/')}>Back to posts</button>
        </div>
    );
};

export default withRouter(Post);
