import React from "react";

export default ({ name, body, email }) => (
    <li>
        <p className='subject'>{name}</p>
        <p className='body'>{body}</p>
        <p className='email'>{email}</p>
    </li>
);
