import React, { Component, Fragment } from 'react';
import axios from 'axios'

import Loader from '../loader';
import Error from '../error';
import Comment from './single-comment';

import postComments from '../../util/post-comment-store';

const CommentList = ({ comments }) =>
    <ul>
        {comments.map((comment, index) =>
            <Comment
                key={index}
                name={comment.name}
                body={comment.body}
                email={comment.email}
            />
        )}
    </ul>;

class Comments extends Component {

    constructor(props) {
        super(props);
        this.state = { loadingState: 'LOADING' }
    }

    componentDidMount() {
        if (postComments[this.props.postId]) {
            this.setState({
                loadingState: 'LOADED',
                comments: postComments[this.props.postId]
            });
        } else {
            axios.get(`https://jsonplaceholder.typicode.com/comments?postId=${this.props.postId}`)
                .then((response) => {
                    postComments[this.props.postId] = response.data;
                    this.setState({
                        loadingState: 'LOADED',
                        comments: response.data
                    });
                })
                .catch(() => { this.setState({ loadingState: 'ERROR' }) });
        }
    }

    render() {
        return (
            <Fragment>
                {this.state.loadingState === 'LOADING' && <Loader />}
                {this.state.loadingState === 'ERROR' && <Error mesage={'Unable to load comments'} />}
                {this.state.loadingState === 'LOADED' && <CommentList comments={this.state.comments} />}
            </Fragment>
        );
    }
}

export default Comments;
