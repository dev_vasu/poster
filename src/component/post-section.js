import React, { Component, Fragment } from 'react';
import { AppStateConsumer } from '../context/app-state';

import PostList from './post-list';
import Footer from './footer';
import Search from './search';

class PostSection extends Component {

    render() {
        return (
            <AppStateConsumer>
                {(context) => (
                    <Fragment>
                        <Search
                            searchText={context.searchText}
                            updateSearchText={context.updateSearchText}
                        />
                        <PostList
                            posts={context.posts}
                            users={context.users}
                            updateUser={context.updateUser}
                            currentPage={context.currentPage}
                            searchText={context.searchText}
                        />
                        <Footer
                            count={context.posts.length}
                            currentPage={context.currentPage}
                            updatePage={context.updatePage}
                        />
                    </Fragment>
                )}
            </AppStateConsumer>
        );
    }
}

export default PostSection;
