import React, { Component } from 'react';
import axios from 'axios'

import PostOverview from '../post-overview';

class PostList extends Component {

    componentDidMount() {
        const userIds = new Set();

        this.props.posts.forEach(post => {
            userIds.add(post.userId);
        });

        userIds.forEach(id => {
            if (!this.props.users[id]) {
                axios.get(`https://jsonplaceholder.typicode.com/users/${id}`)
                    .then((response) => { this.props.updateUser(id, response.data); })
                    .catch(() => { console.log('Error'); });
            }
        });
    }

    render() {
        return (
            <ul className='posts'>
                {
                    this.props.posts
                        .filter(post => post.title.includes(this.props.searchText))
                        .slice((this.props.currentPage * 5), ((this.props.currentPage + 1) * 5))
                        .map(post =>
                            <PostOverview
                                key={post.id}
                                postDetails={post}
                                userDetails={this.props.users[post.userId]}
                            />
                        )
                }
            </ul>
        );
    }
}

export default PostList;
