import React from 'react';

const Context = React.createContext();

export default Context;
export const AppStateProvider = Context.Provider;
export const AppStateConsumer = Context.Consumer;